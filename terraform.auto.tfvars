region = "ap-northeast-1"
ec2_name = "dave EC2"
subnet_id = "subnet-0d541061d7d51e563"
security_groups = ["sg-04206f3322d2cbdcb"]
key_pair = "demo-tokyo-bastion-key"
instance_type = "t2.micro"
assign_ip = false
